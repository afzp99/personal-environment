# Ansible

### Andrés Felipe Zapata Palacio

## Encrypt Variables

    $ ansible-vault encrypt_string --vault-id <env>@<vault-password script> --stdin-name '<variable name>' 

## Decrypt Variables

    $ echo '$ANSIBLE_VAULT;1.1;AES256;
	  3333333333333333333333333333333333333333333333333333333
	  3333333333333333333333333333333333333333333333333333333
	  3333333333333333333333333333333333333333333333333333333
	  3333' | ansible-vault decrypt
