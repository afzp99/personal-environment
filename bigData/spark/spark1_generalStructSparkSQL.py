#################################################
# Uncompleted
#######################################
# 1. Define object struct
case class Person(name: String, age:Int)
# 2. Create RDD from data
val people = sc.textFile("data1_libraries.txt").map().map(p => Person(p(0), p(1).trim.toInt))
# 3. Create RDD of Rows from original RDD
val schemaString = "name age"
# 4. Create Schema represented by a StrictType matching schemaString
val schema = StructType( schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)
# 5. Apply schema to RDD
val rowRDD = people
peopleSchemaRDD.registerTempTable("people")
###############
# Way 2
###############
# Define the schema
# Register RDD as a table
people.registerTempTable("people")
# Run SQL statements
val teenagers = sqlContext.sql("")
