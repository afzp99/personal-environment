# Spark Personal Notes

## Characteristics
   Parallel distributed processing.  
   **Inmemory computing:** Usage of inMemory data structures   
   **Fault tolerant:** If a partition is lost, will be recomputed using trasformations that originally created it.  
   
## Spark Unified Stack

#### Higher Layer
**Spark SQL**
Allows developers to intermix SQL with Spark Language. SchemaRDD can be created using reflection to infer the schema, or suing a programmatic interface to construct a schema and then apply it to he RDD.  

    from pyspark.sql import SQLContext

**Spark Streaming:** Provides processing of live stream of data in small batches. DStreams are sequences of RDDs from a stream of data.  

    import org.apache.spark._
    import org.apache.spark.streaming._
    import org.apache.spark.streaming.StreamingContext._
    
    # Create Streaming Context
    val conf = new SparkConf().setMaster("local[2]").setAppName("name")
    val ssc = new StreamingContext(conf,Seconds(1))
    # Create a DStream
    val lines = ssc.socketTextStream("localhost",9999)
    val words = ...

**MLlib:** Provides machine learning algorithms to scale out across the cluster: Classifications, Regression, CLustering, etc.  

**Graphx:** Graph processing Lib, social networks and language modeling. View data as a graph or as collections (RDD) without data movement or duplications.  

#### Spark Cluster

**Driver (Spark Context):** Schedules tasks on the cluster.
**Cluster Manager:** Standalone, Apache Mesos, Hadoop YARN.
**Executors:** Isolated (You can't share data across applications.

## Spark Configuration
**Spark Properties:** Application parameters can be set using SparkConf object, or dinamically, creating an empty SparkConf() object
for the context, passing the parameters through execution line.  

*Static*  

    val conf = new SparkConf().setMaster("local").setAppName("CountingSheep").set("spark.executor.memory","1g")
       
*Dinamic*  

    ./bin/spark-submit --name "My App" --master local[4] --conf "spark.executor.extraJavaOptions= ..." myApp.jar

**Environment Variables:** Through *conf/spark-env.sh*  
**Logging Properties:** Through log4j.properties in SPARK_HOME/conf/  

## Resilient Distributer Dataset (RDD)
Primary core abstraction. It's a distributed collection of elements parallelized across the cluster.  

Can be created in three ways:  

1) By Existing data in Spark calling the parallelized method, that returns a pointer to the RDD.  
2) Reference a dataset (HDFS, Cassandra, HBase, Amazon S3,etc)  
3) Transform an existing RDD.  

#### Example creating an RDD
     $ $SPARK_PATH/bin/spark-shell  
     > val data = 1 to 10000
     > val data = sc.textFile("hdfs://data.txt")
     > val distData = sc.parallelize(data)
     > distData.filter(...)


#### Operations

**Transformations:** Doesn't return a value, generates & Updates DAG(Direct Acyclic Graphs), the dataset is not loaded into memory until the fist action is executed.  

| Transformation                  | Return                                                                    |
|---------------------------------|---------------------------------------------------------------------------|
| map(f)                          | Dataset passing each element through a function f                         |
| filter(f)                       | Elements of the source if returns true beeing evaluated by the function f |
| flatMap(f)                      | Returns a seq rather than a single item (Lists of lists)                  |
| join(dataset)                   | (k,v) & (k,w) -> (k,(v,w))                                                |
| reduceByKey(f)                  | Aggregates the values for each key using the function f                   |
| sortByKey(ascending|descending) |                                                                           |

**Actions:** (When transformations get evaluated)  

| Action     | Return                          |
|------------|---------------------------------|
| collect()  | Array with all dataset elements |
| count()    | Number of elements              |
| first()    | First element                   |
| take(n)    | Array with first n elements     |
| foreach(f) | Run function f on each element  |

**Persistence:**

**persist()** let you choose persistence level (Disk,memory) as serialized objects (Space efficient but more CPU intensive to read)  
**cache()** deserialized objects in memory.  

Example:  
     > val lines = sc.textFile("hdfs://data.txt") 
**1)** Data is partitioned into blocks across the cluster  
     > val lengths = lines.map(s => s.length)  
     > val totalLengths = lengths.reduce((a,b) => a + b )  
**2)** The driver sends the code to be executed on each block (transformations & actions).  
**3**) Executors read HDFS blocks to prepare the data for the parallel operations.  
**4)** The results are stored, a cache is created.  
**5)** For next actions, HDFS is no accessed, info is in memory.  
**6)** Results go back to the driver.  

## Shared Variables

#### BroadCast
Read-only cached on each machine. Useful to give every node a copy of a large dataset efficiently.
#### Accumulators
Only readable by the Driver.

## Spark Context
Main entry point for Spark functionality. Represent the connection to the cluster.  

**Python**  

       from pyspark import SparkContext, SparkConf
       
       conf = SparkConf().setAppName(name).setMaster(master)
       sc = SparkContext(conf=conf)

**Scala**  

        val conf = new SparkConf().setAppName(name).setMaster(master)
        new SparkContext(conf)

**Java**  

        import org.apache.spark.api.java.JavaSparkContext
        import org.apache.spark.api.java.JavaRDD
        import org.apache.spark.SparkConf
        
        SparkConf conf = new SparkConf().setAppName(name).setMaster(master);
        JavaSparkContext sc = new JavaSparkContext(conf);

## Passing Functions

**1)Anonymous Functions:**  

        (x:Int) => x + 1
        
**2)Static Methods in global Singleton Object:**  

        object MyFunctions {
          def func (s: String): String = {...}
        }
        
        myRdd.map(MyFunctions.func)

**3)Passing by reference:**

        def func(rdd: RDD[String]) : RDD[String] = {...}

## Spark Monitoring
**Web UI:** Port 4040.  
**Metric:** Based on Coda Hale Metrics Library.  
**External Instrumentations:** Ganclia.  

## Spark Tunning
**Data Serialization:** Reduce space in memory.  
**Memory Tunning:** Increasing the level of parallelism will resolve the issue of OutOfMemoryError.


## Usage

#### Spark Submit
**Recommendation:** Don't hardcode the master URL in code, pass it though parameters as shown bellow.  
**Deploy Mode:** Cluster (Worker nodes) or Client (Locally - DEFAULT).  


     sh> $SPARK_HOME/bin/spark-submit \
         --class <main-class>         \
         --master <master-url>        \
         --deploy-mode <deploy-mode>  \
         ... # other options
         <application-jar>            \
         [app-arguments]

#### With Scala

    $ ./bin/spark-shell  
    scala> cal textFile = sc.textFile("file.txt") // Creates a RDD from text file  
     
#### With Python
  
      $ ./bin/pyspark  
      >>> textFile = sc.textFile("file.txt")  

## References

Spark Fundamentals I - CognitiveClass
