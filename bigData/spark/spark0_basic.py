import spark.sparkContext as sc

readme = sc.textFile("README.md")

#With Lambda Function
readme.map(lambda line: len(line.split())).reduce(lambda a, b: a if (a > b) else b)

#With Python Function
def max(a, b):
 if a > b:
    return a
 else:
    return b
    
readme.map(lambda line: len(line.split())).reduce(max)

#Map Reduce Example
wordCount = readme.flatMap(lambda line: line.split()).map(lambda word: (word, 1)).reduceByKey(lambda a, b: a+b)
mostFreqWord = wordCount.reduce(lambda a, b: a if a[1]>b[1] else b)
print(mostFreqWord)