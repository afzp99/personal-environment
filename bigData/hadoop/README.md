# Hadoop

#### Andrés Felipe Zapata Palacio

## HDFS
#### Manage
     $ hdfs dfs -ls /dir
     $ hdfs dfs -mkdir /dir
     $ hdfs dfs -rm -r /hdfs/recursive/remove
#### Copy to HDFS
     $ hdfs dfs -copyFromLocal /local/file /hdfs/dir/
     $ hdfs dfs -put /local/file /hdfs/dir/
#### Copy from HDFS
     $ hdfs dfs -copyToLocal /hdfs/file /local/dir/
     $ hdfs dfs -get /hdfs/file /local/dir/