/**
 * Transfer money from one account to another
 * @transaction
 * @param {} accountTransfer - the account transfer
 */
async function accountTransfer(tx) {

	if(accountTransfer.from.balance < accountTransfer.amount){
		throw new Error('Insufficient money!!!!');
	}

	accountTransfer.from.balance -= accountTransfer.amount;
	accountTransfer.to.balance += accountTransfer.amount;

	return getAssetRegistry()
		.then(function(assetRegistry){return assetRegistry.update(accountTransfer.from);})
		.then(function(){return getAssetRegistry('org.acme.sample.Account');})
		.then(function(assetRegistry){return assetRegistry.update(accountTransfer.to);})
	
    // Save the old value of the asset.
    //const oldValue = accountTransfer.asset.value;

    // Update the asset with the new value.
    //accountTransfer.asset.value = accountTransfer.newValue;

    // Get the asset registry for the asset.
    //const assetRegistry = await getAssetRegistry('org.example.basic.SampleAsset');
    // Update the asset in the asset registry.
    //await assetRegistry.update(accountTransfer.asset);

    // Emit an event for the modified asset.
    //let event = getFactory().newEvent('org.example.basic', 'SampleEvent');
    //event.asset = accountTransfer.asset;
    //event.oldValue = oldValue;
    //event.newValue = accountTransfer.newValue;
    //emit(event);
}
