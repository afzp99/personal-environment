# Blockchain (BC)

Business network. Provides a low level interface for business applications.  
A linked list of blocks, each block describes a set of transactions.  

#### Andrés Felipe Zapata Palacio

## Components & Concepts

**Asset(Activo):** What is transferred (Cars, properties, cash).  
**Ledger(Libro de cuentas electrónico):** Each member of the network registers transactions of Smart contract.  
**World state:** Key Value Database that stores the current state of the assets.  
**Smart Contract(Chain Code):** An automatic Program that defines the transactions. Encapsulates the business rules & business logic.  
**Membership:** Authenticates, manages identities. Enrollment certificate & Transaction certificate.  
**Events:** Creates notifications(block creation | smart contracts).  
**Wallet:** Manages security credentials.  
**Consensus:** The process of maintaining a consistent ledger (Keeps peers up-to-date & Fix errors & Quarantine all malicious nodes).  

### Consensus typical flow

* The App Submits a request to invoke a transaction.  
* The transaction is shared around the network.  
* A peer creates a block containing the transaction.  
* Block's transactions are executed.  
* The net attemprs to agree the correct result.  
* The correct output is applied to the wold state if there is agreement.  

### Consensus algorithms

* Proof of work.  
* Proof of stake.  
* Solo (No consensus).  
* Kafka & Zookeeper.  
* Proof of Elapsed Time.  
* PBFT-based.  

## Benefits

**World State DB:** Stores the actual state of the assets *(Ej: Owner)*.  
**Hashes:** Shows inconsistence between blocks.  
**Better than Centralized DB:** BC Doesn't have single point of failure.  
**Traceability:** You can trace the whole transactions over an asset. Provides provenance for an asset.  
**Append only:** A register is never deleted, in case of inconsistance, a new register is created.  
**Tamper-proof:** (A prueba de Sabotages).  
**Smart Contract:** Business rules implied by the contract embedded in blockchain.  
**Identity:** Supported by a cryptographic certificate (Public Key & Private Key).  
**Confidentiality:** Channels are independent of each other. The transaction is visible only for the channel members.  
    
## Private VS Public

**Private**

    Ej: Hyperledger Fabric.
	Network Members are known.
	Transactions are secret.
	
**Public**

    Ej: Bitcoin.
	Participant identity is almost impossible to infer.
	Transactions are public.
	
## Use cases

* Cryptography (Bitcoin)
* Business network
* Food trust (Trace from farm to market)
    
## Problems

* Each member has it's own security level (Vulnerable)
* Private Keys visible for Admins -> An admin can see transactions -> Hardware Security Module (Stores those private Keys)

## Hyperledger Composer

Application development framework that helps creating a **Hyperledger Fabric Applications**. You can use it to deploy business
networks and applications that allow participants to make transactions and exchange assets in a network.

#### Structure

**Access Control Rules(ACR):** AC Language.  
**Business Network Definition (BND):** Defines data model, transaction logic & ACR.

#### Roles

**Committing peer:** Commits transactions. MAY HOLD Smart Contract.  
**Endorsing Peer:** Recieves a transaction proposal and grats or denies it. MUST HOLD Smart Contract.  
**Ordering Nodes:** Approves the inclusion of transaction blocks into the ledger. DOESN'T HOLD Smart Contract. DOESN'T HOLD Ledger.  

#### From transaction to Block

**1.** Application proposes transaction (Endorsement policy).  
**2.** Endorsers execute transaction proposal.  
**3.** Application recieves responses from endorsing peers.  
**4.** Application submits responses as a transaction to be ordered by Ordering Nodes.  
**5.** Ordering service delivers the block to all commiting peers.  
**6.** Commiting peers validates against the endorsement policy.  

    NOTE: Invalid transactions are also retained on the ledger but don't update world state.
	
**7.** THe application is notified by each peer.  

## References
IBM Blockchain Foundation Developer Course - Cognitive Class.  
IBM Educative content.  
