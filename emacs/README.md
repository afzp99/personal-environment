# Emacs

## Useful commands

    C-x 0  Close current window without closing the others
    M-<    Inicio del buffer
    M->    Fin del buffer
    M-%    Replace
