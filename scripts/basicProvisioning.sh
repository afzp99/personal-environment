#!/bin/bash

declare -r script_path=`readlink -e $0`
declare -r scripts_dir=`dirname $script_path`
declare -r repo_dir="${scripts_dir%/*}"

# Sets basic personalized functions & variables
source $scripts_dir/essentials.sh 

print $BLUE "[Synchronizing Config Files]"
print $GREEN "> commonrc"
(cp $repo_dir/configFiles/commonrc ~/.commonrc)||(err)
print $GREEN "> bashrc"
(cp $repo_dir/configFiles/bashrc ~/.bashrc)||(err)
print $GREEN "> zshrc"
(cp $repo_dir/configFiles/zshrc ~/.zshrc)||(err)
print $GREEN "> gitconfig"
(cp $repo_dir/configFiles/gitconfig ~/.gitconfig)||(err)
print $GREEN "> emacs"
(cp $repo_dir/emacs/emacs.cfg ~/.emacs)||(err)
print $GREEN "> emacs packages"
(cp $repo_dir/emacs/packages.cfg ~/.emacs.d/packages.cfg)||(err)
