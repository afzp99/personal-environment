BOLD="\e["
NORMAL="\e[0m"
RED="\e[31m"
GREEN="\e[92m"
YELLOW="\e[33m"
BLUE="\e[34m"
MAGENTA="\e[35m"

print(){
    echo -e "$@"$NORMAL
}

err(){
    print $RED "ERROR"
}
