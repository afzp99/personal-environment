EMACS_URL=http://ftp.gnu.org/gnu/emacs/
EMACS_FILE=emacs-26.1.tar.gz
TEMP_DIR=/tmp/emacs_install

function main(){
    mkdir -p $TEMP_DIR
	cd $TEMP_DIR
	wget $EMACS_URL
	tar xvf $EMACS_FILE
	rm -rf $TEMP_DIR
}

main()

#sudo apt install build-essential
# get all dependencies of a previous emacs version
#sudo apt build-dep emacs
# Get source
#cd /tmp
#git clone https://github.com/emacs-mirror/emacs.git
# Go to source and build
#cd emacs
#./autogen.sh
#./configure
#make -j4
# Install (optional)
#sudo make install
