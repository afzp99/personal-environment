#!/bin/bash

declare -r machine="$1"
declare -r script_path=`readlink -e $0`
declare -r script_dir=`dirname $script_path`
declare -r repo_dir="${script_dir%/*}"
declare -r cfg_file="$repo_dir/ansible.cfg"
declare -r inv_file="$repo_dir/inventory/hosts"

declare -r machines=`ls $repo_dir/playbooks/`
if [[ "$machines" != *"$machine"* ]]; then
    echo -e "\nUnrecognized machine. Choose from:\n$machines\n"
    exit 1
fi

# ansible-vault requires pycrypto 2.6, which is not installed by default
# on RHEL6 based systems.
declare -i centos_version=`rpm --query centos-release | awk -F'-' '{print $3}'`
if [[ "$centos_version" -eq "6" ]]; then
    /usr/bin/yum --enablerepo=epel -y install python-crypto2.6
fi

/usr/bin/yum --enablerepo=epel -y install ansible

# Run ansible
export ANSIBLE_CONFIG="$cfg_file"
export DEFAULT_ROLES_PATH="$repo_dir/roles"
ansible-playbook \
    --inventory-file="$inv_file" \
    --extra-vars "machine=$machine repo_dir=$repo_dir" \
    --vault-id "$machine@$repo_dir/scripts/vault-secrets-client.sh" \
    $repo_dir/site.yml \
    -vvv
