#!/bin/bash

source essentials.sh

declare -r script_path=`readlink -e $0`
declare -r repo_dir=`dirname $script_path`
declare -r cfg_file="$repo_dir/ansible/ansible.cfg"
declare -r inv_file="$repo_dir/ansible/inventory/hosts"

print $MAGENTA "[Installing Ansible]"
print $YELLOW ">> Updating APT Cache <<"
sudo apt update
print $BLUE ">> Installing prerequisits <<"
sudo apt install -y software-properties-common

print $YELLOW ">> Adding Ansible repository <<"
sudo apt-add-repository -y ppa:ansible/ansible
print $YELLOW ">> Updating APT Cache <<"
sudo apt update
print $GREEN ">> Installing Ansible Package <<"
sudo apt install -y ansible

# Run ansible
#export ANSIBLE_CONFIG="$cfg_file"
#rm -rf /etc/ansible
#cp -r ansible/ /etc/ansible
#cd
#ansible-playbook --inventory-file="$inv_file"\
#		 --ask-vault-pass  $repo_dir/site.yml
