# Personal Environment

#### Andrés Felipe Zapata Palacio

## MiniConda
#### 0. Bzip2 Installed
     $ yum install bzip2
#### 1. Download and install Miniconda
     $ curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
     $ ./Miniconda3-latest-Linux-x86_64.sh

#### 2. Create environment
     $ conda create -n <envName> [package_0 ... package_N]
     $ conda create -n exampleEnv numpy
     $ conda create -n name python=2.7
#### 3. Packages
     $ conda install numpy  (In the current environment)

#### 3. Remove environment
     $ conda env remove -n <envName>

## EMACS
#### 1.

## GIT
#### 1. Enable colors

     git config --global color.ui auto

#### 2. Update fetch & push URL after adding SSH Key

     git remote set-url --push origin git@gitlab.com:afzp99/personal-environment.git
     git remote set-url origin git@gitlab.com:afzp99/personal-environment.git
     git remote -v
     
#### 3. Store changes temporarly & Apply them (Useful for changing current branch or pull without merging)
    
     git stash
     git stash apply
     
#### 4. Push unexisting branch to Origin

     git push --set-upstream origin <newBranch>
	 
#### 5. Create a new Branch and change to it

     git checkout -b new-branch original-banch

## SSH
#### 1. Create Trust
     cd ~/.ssh/
     ssh-keygen -t rsa
     ssh-copy-id user@host
