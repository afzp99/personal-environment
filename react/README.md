## NPM

Add dependency to package.json

    npm install <PKG> --save-option
	
Just for development environment

    npm install <PKG> --save-dev

## React Vocabulary

* One way data flow
* JSX
* Components
* State
* Virtual DOM
* Flux
* Redux

## React Component Lifecycle ##

**1) Mounting:** Included into the DOM (Hierarchy)

    constructor()
    getDerivedStateFromProps()
	render()
	componentDidMount()
	
**2) Updating:** Component being re-rendered

	getDerivedStateFromProps()
	shouldComponentUpdate()
	render()
	getSnapshotBeforeUpdate()
	componentDidUpdate()

**3) Unmount:** Removed from view



## Component Classification ##

( Presentational vs Container )

    1) Concerned with render based on props data, don't maintain own state.
    2) Responsible for data fetching and state updates. Use presentational components for rendering. Provides data to presentational components. Maintain state and communicate with data sources.

( Skinny vs Fat )  

( Dumb vs Smart )  

( Stateless vs Stateful )  


