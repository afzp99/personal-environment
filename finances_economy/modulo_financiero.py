class modulo_financiero:
  # Cuánto pago en Intereses?
  def interes_simple(k,n,i):
    return k*n*i
  
  def interes_compuesto(k,n,i):
    return k*(1+i)**n-k
  
  # Tasas
  def tasaEfectiva(nominal,n,frecCapitalizacion=0):
    if frecCapitalizacion == 0: frecCapitalizacion = n
    return (1+nominal*frecCapitalizacion/n)**n-1
  #TO_TEST
  def anticipada_a_efectiva(ia):
    return ia/(1-ia)
  #TO_TEST
  def efectiva_a_anticipada(ie):
    return ie/(1+ie)
  #TO_TEST
  def tasaReal(ie,inflacion):
    num=1+ie
    den=1+inflacion
    return (num/den)-1

  # Pagos Unicos
  def futuro(p,i,n):
    return p*(1+i)**n
  #TO_TEST
  def presente(f,i,n):
    return f/(1+i)**n

  def formatter(val):
    return '{:0,.2f}'.format(val)

  def printd(val):
    print(modulo_financiero.formatter(val))
  
  # Series
  #TO_TEST
  def calc_anualidad_desde_pasado(p,i,n):
    num=p*i*(1+i)**n
    den=(1+i)**n-1
    return num/den
  #TO_TEST
  def calc_anualidad_desde_futuro(f,i,n):
    num=f*i
    den=(1+i)**n-1
    return num/den
  
  def presente_anualidades(a,i,n,g=0):
    grad= g * (1/i - n/((1+i)**n-1))
    part2 = ((1+i)**n-1)/((1+i)**n*i)
    return (a + grad )* part2

  def futuro_anualidades(a,i,n,g=0):
    grad= g * (1/i - n/((1+i)**n-1))
    part2 = ((1+i)**n-1)/i
    return (a + grad )* part2
    return num/den

  # Series no uniformes

  def presente_anualidades_grad_geometrico(a,i,n,j):
    presente = a + (1-((1+j)/(1+i))**n)/(i-j)
    return presente

  def futuro_anualidades_grad_geometrico(a,i,n,j):
    futuro = modulo_financiero.futuro(modulo_financiero.presente_anualidades_grad_geometrico(a,i,n,j),i,n)
#    futuro = a + (1-((1+j)/(1+i))**n)/(i-j)
    return futuro
