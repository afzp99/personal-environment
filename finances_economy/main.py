from modulo_financiero import modulo_financiero as mf
import matplotlib.pyplot as plt

#iem=0.021
#a=45000000
#f_12=mf.futuroPagoUnico(a,iem,12)
#f_A_11=mf.futuroAnualidad(a,iem,11)
#f_A_12=mf.futuroPagoUnico(f_A_11,iem,1)

#print("VF="+formatter(f_12)+"+"+formatter(f_A_12))

p = 10000
i=0.01
lista = [
[mf.calc_anualidad_desde_pasado(p,i,36),36],
[mf.calc_anualidad_desde_pasado(p,i,96),96],
[mf.calc_anualidad_desde_pasado(p,i,160),160],
[mf.calc_anualidad_desde_pasado(p,i,280),280],
[mf.calc_anualidad_desde_pasado(p,i,420),420],
[mf.calc_anualidad_desde_pasado(p,i,480),480]]
plt.plot([x[1] for x in lista],[x[0] for x in lista],'r')
plt.show()

# p = 500000000
# i=0.017
# lista = [
# [mf.calc_anualidad_desde_pasado(p,i,36),36],
# [mf.calc_anualidad_desde_pasado(p,i,96),96],
# [mf.calc_anualidad_desde_pasado(p,i,160),160],
# [mf.calc_anualidad_desde_pasado(p,i,280),280],
# [mf.calc_anualidad_desde_pasado(p,i,420),420],
# [mf.calc_anualidad_desde_pasado(p,i,480),480]]
# plt.plot([x[1] for x in lista],[x[0] for x in lista],'r')
# plt.show()

#f = 255000000
#intrim=4.25/100
#inmensual= mf.tasaEfectiva(intrim,1/3)
#a = mf.calc_anualidad_desde_futuro(f,inmensual,2*12)
#mf.printd(a)
#p = mf.presente(f,inmensual,2*12)
#mf.printd(p)

# inmensual = 2.25/100
# n = 18 #meses

# a1 = 2000000
# p1_1 = mf.presente(a1,inmensual,1)
# p1_2 = mf.presente(a1,inmensual,3)
# p1_3 = mf.presente(a1,inmensual,6)
# p1_4 = mf.presente(a1,inmensual,9)
# p1_5 = mf.presente(a1,inmensual,12)
# p1_6 = mf.presente(a1,inmensual,15)
# p1_7 = mf.presente(a1,inmensual,18)
# p1 = p1_1+p1_2+p1_3+p1_4+p1_5+p1_6+p1_7

# a2 = 5000000
# p2_1 = mf.presente(a2,inmensual,7)
# p2_2 = mf.presente(a2,inmensual,13)
# p2_3 = mf.presente(a2,inmensual,19)
# p2 = p2_1 + p2_2 + p2_3

# a3 = 2500000
# n3 = 6
# i3 = mf.tasaEfectiva(inmensual,n3)
# veces3 = 3
# p3 = mf.presente_anualidades(a3,i3,veces3)

# p = p1 + p2 - p3
# mf.printd(p)

# a= 500000
# i=1.01/100
# n=12
# g=200000
# VP = mf.presente_anualidades(a,i,n,g)
# VF = mf.futuro_anualidades(a,i,n,g)

# print("VP=", end="")
# mf.printd(VP)
# print("VF=", end="")
# mf.printd(VF)

# a2 = a + g*7
# VP7_1 = mf.presente_anualidades(a2,i,5,g)
# VP7_2 = mf.futuro_anualidades(a,i,7,g)
# VP7 = VP7_1 + VP7_2
# print("VP7:", end="")
# mf.printd(VP7)
####################################
# a_sem=25e6
# a_trim=8e6
# i_anual = 14.75/100
# i_semestral = mf.tasaEfectiva(i_anual,1/2)
# j=3/100
# n = 5 #años
# i_trim = mf.tasaEfectiva(i_anual,1/4)
# P_no_uniforme =mf.futuro_anualidades_grad_geometrico(a_trim,i_trim,n*4,j)
# P_uniforme = mf.presente_anualidades(a_sem,i_semestral,n*2)
# mf.printd(P_uniforme)
