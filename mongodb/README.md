# MongoDB #

## Concepts ##

* Database => Collections => Documents => Nested Documents | Values | Arrays
* A namespace is defined as database.collection
* Query example: { "name":"Pepito" }
* Query example 2: { duration: {$gte: 60,$lt: 65}}
* Query specific fields: find({},{_id:0})

## Mongo Shell ##

    > show dbs
	> use document
	> show collections
	> db.movieDetails.find().pretty()
	
Insert

    > db.movies.insertOne({title: "Star Wars", year: 2019})
	> db.movies.insertMany(
	[
		{
		"title": "Star Wars",
		"year": 2019
		},
		{
		"title": "Star Wars II",
		"year": 2020
		}
	])

Array Query

	Array cast[0] = "Pepito Perez"
    > {cast.0: "Pepito Perez"}
	Pepito Perez is in cast
	> {cast: "Pepito Perez"}
	Cast is exactly the value
	> {cast: ["Pepito Perez"]}
	
**Advanced Operators**

a) Movie's genre is in the given array (action or drama)

    > db.movies.find({ "genre":{ $in: ["action","drama"] } })
    
b) Conditionals (or | and)

    > db.movies.find({ $or: [ conditional1, conditional2 ]})
       > Ex: conditional1 -> "name": "Harry Potter"
