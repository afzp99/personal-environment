# MongoDB for Python Developers #

## Structure ##

    import pymongo
	uri = "mongodb+srv://m220-user:m220-pass@m220-lessons-mcxlm.mongodb.net/test"
	client = pymongo.MongoClient(uri)
	m220 = client.m220
	movies = m220.movies
	movies.find_one()
	movies.find_one( { "cast": "Salma Hayek" } )
	movies.find( { "cast": "Salma Hayek" } ).count()

## Queries ##

Limit number of results

    movies.find().limit(N)
	
Sorting

    movies.find().sort("year", ASCENDING)
    movies.find().sort([("year", ASCENDING),("title", ASCENDING)])
	
## Aggregation Framework ##
	
Pipeline that collects, filters and process query results.

    pipeline = [
		{ "$match": { "cast": "Tom Hanks"} },
		{ "$project": { "_id": 0, year:1, "title":1, "cast":1 } },
		{ "$sort": { "year": ASCENDING, "title": ASCENDING } }
	]
	
	sorted_aggregation = movies.aggregate(pipeline)
	print(dumps(sorted_aggregation, indent=2))


## Insert ##

    insert_results = bd.movies.insert_one({"name":"HarryPotter", "year":2025})
	insert_results.acknowledged                  # True or False
    inserted_id = insert_result.inserted_id      # Object Id
	bd.movies.find_one({"_id": inserted_id})
	
## Update ##

    new_info = {"name":"HarryPotter", "year":2010}
	update_results = db.movies.update_one({"title":"HarryPotter"},{"$set": new_info}, upsert=True)

## Join ##

    {
		from: 'comments',
		let: { 'id': '$_id' },
		pipeline: [
			{ "$match":
				{ '$expr' : { '$eq': [ "$movie_id", "$$id" ]} }
			}
		], as 'movie_comments'
	}
	
## Bulk Writes ##

**Secuential:** Specific order

    db.stock.bulkWrite([
	{updateOne:{...}},
	{...}
	]
	)
	
**Parallel**

    db.stock.bulkWrite([...],
	{ordered:false})
