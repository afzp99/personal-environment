# DevOps

## Definitions

* Emerging professional movement
* Creates a collaborative working relationship between Development & IT Operations.
* Generates fast flow of planed work.
* It's about people & creating a culture of focusing on delivering value for the customer **(Courtney Kissler)**.  

## Roles

**Development**
**Test:** Testing is the new planning.  
**Operations:** Create resources to keep running the software
(standarize & automated infraestructure).  

## CAMS & CALMS

**C (Culture):** Performance oriender, High coop, shared risks.
(Generative Culture - Ron Westrum - Organizational Cultures Topology).  
**A (Automation):**  
**L (Lean):** More info below.  
**M (Measurement):** Quality.  
**S (Sharing):** ideas between the Dev & Ops teams.  
	 
## The Three Ways (Gene Kim & Mike Orzen)

**1. Systems Thinking:** Performance of entire system, not individual silos.  
**2. Amplifying feedback loops:** Allows reflection before determining next 
    steps after the output of a stage. Improve Feedback loops building automated tests
	into pipeline.  
**3. Culture of Continuous experimentation & Learning:** Allocating time for
	improvement work. Create a culture of Innovation & testing-learning capacity.  

## Seven Principles of Lean

**1. Eliminate Waste:** Don't code more features than needed.
**2. Building Quality In:** Everyone's responsability.
**3. Creating Knowledge:** Development is constant learning (Feadback loops).  
**4. Postpone (Defer) Commitment:** Take desitions with enough information.  
**5. Deliver Fast:** Receive feedback early & often.  
**6. Respect People:** CULTURE.  
**7. Optimize the Whole:** Systems thinking.  

## Seven Wastes of Lean in Software Development

**1. Partially Done Work:** Untested code, Undocumented code. Delays delivery of value.  
**2. Extra Features:** 
**3. Revisiting Decisions:** 
**4. Handoffs:** Transfer tasks or responsabilities.  
**5. Delays:** Process has to wait to proceed (Compiling).  
**6. Task Switching:** Multitasking is the enemy of all productivity!!!  
**7. Defects:** Build quality is important.  

## Improvement Kata

**1.** Understand vision & direction for project. Where you wanna go.  
**2.** Analyze to understand current condition. Where you are.  
**3.** Establish target condition. Set target.  
**4.** Plan > Do > Check > Act (A3 Problem Solving Framework)  

**(PLAN)**  
**Background:** How problem impacts business results.  
**Current Condition:** Quantify. Create a problem statement.  
**Target Condition:** Measure results.  
**Analysis:** Identify root cause and factors.  
**(DO)**  
**Brainstorm:** How you intend to reach target. Countermeasure that helps to reach goal.  
**(CHECK)**  
**Implementation Plan:** Check results that confirms the impact.  
**(ACT)**  
**Follow Up Actions:** Update standard work.

## References

* Coursera - DevOps Culture and Mindset (University of California)
* Coursera - Continuous Delivery & DevOps
