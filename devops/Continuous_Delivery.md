# Continuous Delivery

## Pipeline Steps

**(Start)** NEW CODE  
1. Commit & Unit Tests: Written by each developer.  
2. Medium Tests (Integration Tests): Crossed functions. Local Network access.  
3. Large Tests (System): Performance, security, non-functional & Functional, remote Network Access, databases.  
4. Manual Validation  
5. Deployment  
**(End)** RELEASE PRODUCT  

## References

* Coursera - Continuous Delivery & DevOps
