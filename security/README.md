# Information Security

#### Andrés Felipe Zapata Palacio

## Generic Vulnerabilities in TCP
**Footprinting:** Extract information about the target in internet, DNS records, etc.  

    ping, whois, traceroute, Nmap, finger, nslookup.

**Fingerprinting:** The objective is to know the OS & service version of the machine, identifying particular characteristics of the TCP/IP implementation.  
Active (Sending packets) or passive (monitoring traffic).  
*Ej: TTL, WindowSize, TOS.*  

    Nmap, QUESO.

**Port Scanning:** Once IP level is reached.

    Nmap, Strobe, Netcat.
    $ nmap -sT <<IP-dirs>>      # Active TCP Ports   
    $ nmap -sU <<IP-dirs>>      # Active UDP Ports   
    $ nmap -O  <<IP-dirs>>      # Try to know OS  
    
**ICMP based scanning:** Echo, Broadcast, Timestamp, ICMP information, address mask (created for routers without HDD, to obtain net info in boot).

**Sniffing:** Network envinomnets based in difusion (Ethernet). Obtaining non enctrypted info, activating a network interface in promiscuous mode, storing the traffic in logs.  

**Eavesdropping:** 

**Snooping:**
