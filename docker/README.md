# Personal Environment

#### Andrés Felipe Zapata Palacio

## Commands
#### 1. Docker
     $ docker ps		   (Show running containers)
     $ docker ps -a		   (Show all containers)
     $ docker images		   (Show all images)
     $ docker rm [ID]	      	   (Removes a container)
     $ docker rm $(docker ps -aq)  (Removes the whole containers)
     $ docker rmi [ID]	     	   (Removes an image)
     $ docker start [ID]
     $ docker stop [ID]
     $ docker build -t [TAG] [DIR] (Builds an image from a dir with a Dockerfile)

#### 2. Docker Compose
     $ docker-compose build	(Initialize the base executing only Dockerfile)
     $ docker-compose up	(Creates the whole services and starts them in foreground)
     # docker-compose start 	(Starts services in background, if they are not created, it won't do anything)


#### 3. Docker Swarm basics
     $ docker swarm init			    (Init as Swarm manager)
     $ docker swarm join --token [TOKEN] [IP:PORT]  (Joins as node)
     $ docker node ls	 	 	 	    (Lists the nodes attached)
     $ docker service ls			    (Lists the running services)
     $ docker swarm leave			    (A node leaves the swarm)
     $ docker node rm (ID|Hostname)		    (A node is removed by master)

#### 4. Docker Swarm
     $ docker stack deploy --compose-file docker-compose.yml [NAME]
     $ docker service ps [SERVICE]	            (Shows remote and local instances)
     $ docker node update --availability (active|pause|drain) [NODE]
     $ docker swarm join-token worker               (Shows the token for joining)

#### 4. Docker Swarm Firewall CentOS 7 Configuration
     $ sudo firewall-cmd --add-port=2376/tcp --permanent  
     $ sudo firewall-cmd --add-port=2377/tcp --permanent  
     $ sudo firewall-cmd --add-port=7946/tcp --permanent  
     $ sudo firewall-cmd --add-port=7946/udp --permanent  
     $ sudo firewall-cmd --add-port=4789/udp --permanent

     $ sudo service firewalld restart