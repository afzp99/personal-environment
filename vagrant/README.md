# Vagrant 

#### Andrés Felipe Zapata Palacio

## Basic Usage
     vagrant up --provision     -> Poweron the vagrant machine and executes the provisioning steps
     vagrant destroy		-> Destroys the machine but preserves the .box (golden image)
     vagrant rsync		-> Sync the directories
