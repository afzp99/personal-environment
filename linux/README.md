# Linux Administration Section

#### Andrés Felipe Zapata Palacio

## Bash
#### 1) awk
Split a string and take its second argument.

    awk -F: '{print $2}'

## Network

#### Open Port using Firewalld

    firewall-cmd --zone=public --add-port=<PORT>/tcp

#### Open Port using iptables

    iptables -A INPUT -p tcp --dport <PORT> -j ACCEPT
	
#### Edit Network Interfaces with a manager in console

     $ nmtui

#### List TCP open ports

    $ netstat -lt
	$ ss -lt
	
#### List UDP open ports

    $ netstat -lu
	$ ss -lu
	
## Nmap

#### Scan everything

    $ nmap -Pn -A host.hostname
	$ nmap -Pn -sU host.hostname     #Scan UDP ports
	
The -Pn treat all hosts as online (In case ICMP is disabled)
