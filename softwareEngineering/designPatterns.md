(Creational Design Patterns)
============================

Singleton
---------
**Def:** Ensures a class has a unique instance to time and provides global access.  
**Advantages:** Centralized management of resources.

    class Singleton{
	  private static Singleton uniqueInstance;
	  
	  getInstance(){
		  if uniqueInstance == null:
			  uniqueInstance = new Singleton();
		  return uniqueInstance;
	  }
	  
    }

Factory
Abstract Factory
Builder
Prototype

(Structural Design Patterns)
============================

Adapter
-----
**Def:**  
**Advantages:**

Composite
-----
**Def:**  
**Advantages:**

Facade
-----
**Def:**  
**Advantages:**

Proxy
-----
**Def:**
**Advantages:**

Interface
---------
**Def:** Separates definition from implementation.  

Bridge
Decorator
Flyweight


(Behavioural Design Patterns)
=============================
Iterator
-----
**Def:**  
**Advantages:**

Observer
-----
**Def:**
**Advantages:**

Strategy
-----
**Def:**  
**Advantages:**

Interpreter
Template Method
Chain of Responsability
Command
Mediator
Memento
State
Visitor

(References)
============

* Design Patterns: Elements of Reusable Object-Oriented Software - The Gang of Four
