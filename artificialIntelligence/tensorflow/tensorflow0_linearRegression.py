#############################################################
# Author: Andrés Felipe Zapata Palacio
# Objective: Create a succesful Linear Regression Module for
#            the function f(x)=x^2
#############################################################

# Common imports
from __future__ import print_function
import math
from IPython import display
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

#################################################################################
# Defining the input function
# Taken from Google Crash Course - ML
#################################################################################
def my_input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    """Trains a linear regression model of one feature.
  
    Args:
      features: pandas DataFrame of features
      targets: pandas DataFrame of targets
      batch_size: Size of batches to be passed to the model
      shuffle: True or False. Whether to shuffle the data.
      num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
    Returns:
      Tuple of (features, labels) for next data batch
    """
  
    # Convert pandas data into a dict of np arrays.
    features = {key:np.array(value) for key,value in dict(features).items()}                                           
 
    # Construct a dataset, and configure batching/repeating.
    ds = Dataset.from_tensor_slices((features,targets)) # warning: 2GB limit
    ds = ds.batch(batch_size).repeat(num_epochs)
    
    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels


def train_model(INPUT_FILE, STEPS, LEARNING_RATE, BATCH_SIZE, NUMBER_OF_PREDICTIONS):
  """Trains a linear regression model of one feature.
  
    Args:
    learning_rate: A `float`, the learning rate.
    steps: A non-zero `int`, the total number of training steps. A training step
      consists of a forward and backward pass using a single batch.
    batch_size: A non-zero `int`, the batch size.
    input_feature: A `string` specifying a column from `california_housing_dataframe`
      to use as input feature.
    """
  # 0.1) Load the data
  function_dataFrame = pd.read_csv(INPUT_FILE, sep=",")
  print("Normal Dataframe")
  print(function_dataFrame)

  # 0.2) Randomize order
  function_dataFrame = function_dataFrame.reindex(
    np.random.permutation(function_dataFrame.index))
  print("ReOrdered Dataframe")
  print(function_dataFrame)
  
  # 1)Define & Configure Feature Columns
  feature_name="x"
  feature_x = function_dataFrame[[feature_name]]
  training_set = feature_x.head(STEPS)
  prediction_set = feature_x.tail(NUMBER_OF_PREDICTIONS)
  numerical_feature = [tf.feature_column.numeric_column(feature_name)]
  
  # 2) Define the Target
  target_name="y"
  targets = function_dataFrame[target_name].head(NUMBER_OF_PREDICTIONS)
  
  # 3.1) Configure Optimizer (Gradient descent)
  my_optimizer=tf.train.GradientDescentOptimizer(learning_rate=LEARNING_RATE)
  my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
  # 3.2) Configure Linear Regressor
  linear_regressor = tf.estimator.LinearRegressor(
    feature_columns=numerical_feature,
    optimizer=my_optimizer)
  
  # 4) Define input Function
  #    DEFINED AT THE BEGINNING AS my_input_fn(...)
  
  # 5) Train the Model
  _ = linear_regressor.train(input_fn = lambda:my_input_fn(training_set, targets, batch_size=BATCH_SIZE),steps=STEPS)
  
  # 6) Evaluate the Model
  # 6.1) Create input function for predictions
  prediction_input_fn =lambda: my_input_fn(prediction_set, targets, num_epochs=1, shuffle=False)
  # 6.2) Make predictions
  predictions = linear_regressor.predict(input_fn=prediction_input_fn)
  # 6.3) Format Predictions to NumPy array
  predictions = np.array([item['predictions'][0] for item in predictions])
  
  # 7) Calculate Squared Error
  mean_squared_error = metrics.mean_squared_error(predictions, targets)
  root_mean_squared_error = math.sqrt(mean_squared_error)
  print("Mean Squared Error (on training data): %0.3f" % mean_squared_error)
  print("Root Mean Squared Error (on training data): %0.3f" % root_mean_squared_error)
  
  comparisson = pd.DataFrame()
  comparisson["predictions"] = pd.Series(predictions)
  comparisson["targets"] = pd.Series(targets)
  print("Comparing results statistics")
  print(comparisson.describe())
  print("***Predicciones vs Realidad***")
  print(comparisson)
  
  sample = function_dataFrame.sample(n=STEPS//2)
  # Get the min and max values.
  x_0 = sample["x"].min()
  x_1 = sample["x"].max()
  
  # Retrieve the final weight and bias generated during training.
  weight = linear_regressor.get_variable_value('linear/linear_model/x/weights')[0]
  bias = linear_regressor.get_variable_value('linear/linear_model/bias_weights')
  
  # Get the predicted y values for the function
  y_predicted_0 = weight * x_0 + bias 
  y_predicted_1 = weight * x_1 + bias

  #y_0 = sample["y"].min()
  #y_1 = sample["y"].max()
  
  # Plot predicted function
  plt.plot([x_0, x_1], [y_predicted_0, y_predicted_1], c='r', color='r', label="Predicted")

  # Plot original function
  # plt.plot([x_0, x_1], [y_0, y_1], c='r', color='y', label="Original")
  
  # Label the graph axes.
  plt.ylabel("f(x)")
  plt.xlabel("x")
  
  # Plot a scatter plot from our data sample.
  plt.scatter(sample["x"], sample["y"])
  # Display graph.
  plt.show()


if __name__ == "__main__":
  # First Set-Up
  tf.logging.set_verbosity(tf.logging.ERROR)
  pd.options.display.max_rows = 10
  pd.options.display.float_format = '{:.1f}'.format
  # Vars
  INPUT_FILE="function_values.csv"
  STEPS=50
  LEARNING_RATE=0.01
  BATCH_SIZE=1
  NUMBER_OF_PREDICTIONS = 50
  # Call function
  train_model(INPUT_FILE,STEPS,LEARNING_RATE,BATCH_SIZE, NUMBER_OF_PREDICTIONS)
