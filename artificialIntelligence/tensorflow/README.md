# Usage

    $ conda create -n tensorflow python=3.6
    $ source activate tensorflow
    $ conda install tensorflow numpy scikit-learn matplotlib IPython pandas

# Tensorflow
Open Source Software Library for numerical computation, allowing users to build various models from scratch.

## Concepts
**Tensor:** n-dimentional matrix.  
**Batch size:** Number of examples per step.  

## Supervised ML assumptions
If these assumptions are violated, the model looses the ability to predict on new data.

**1)Randomness:** Examples don't influence each other (independency).
**2)Stationary:** Distribution doesn't change within the dataset
**3)Same districution**

## Levels
**High:** TF Estimators, ObjectOriented API, Compatible with Scikit-learn  
**Libraries commmon model components:** tf.layers, tf.losses, tf.metrics  
**Python TF:** Wrap C++ Kernels  
**C++ TF:**  
**Hardware:** CPU, GPU, TPU  

## Components
**Graph Protocol Buffer:**  
**Runtime:** Executes the graph  

## Type of Features (Input Data):
**Categorical Data:** Textual. Exceptions like postal code, numerical data that should be treated as categorical.  
**Numerical Data:** Int | Float.  

## Functions:

**tf.Session()** Encapsulates the environment in which Tensor models are evaluated. It's important to close the session at the end.  
**tf.InteractiveSession()** Same as Session but sets itself as the default one.  

    with tf.Session() as sess:  
      result = sess.run(...)  
   
Or

    session = tf.Session(graph=g)  
	result = session.run(...)
    session.close()  

**tf.Graph()** Creates the graph.  

**tf.constant(value, dtype, shape, name, verify_shape)**  

**tf.fill(dimentions, value, name)**  

**Control dependencies:** The graph has 3 ops: a,b,c. 'c' will only run after 'a' & 'b'  

    g.control_dependencies([a,b]):
        c = ...
        
**tf.gradients(y,[xs])** Take derivative of y with respect to each tensor in the list xs  

#### Checkpointing

**tf.train.Saver** This class Saves graph's variables in binary files.  
**max_to_keep** Default 5, if None or 0, all checkpoint files are kept.  
**keep_checkpoint_every_n_hours** Default 10.000 hours.  
**tf.train.Saver.save(sess, save_path,...)** Saves sessions, not graphs.  
**saver.restore(sess, PATH)** Restore variables.  

#### Global Step

**self.global_step** = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')  
**self.optimizer** = tf.train.GradientDescentOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)  

#### Convultions
**Padding:** VALID(No padding) & SAME (Padding with Zeros)  

    conv1 = tf.layers.conv2d(inputs=imagen, filters, kernel_size, padding, activation=tf.nn.relu, name='conv1')
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2,2],stride=2, name='pool1')
    ...
    fc = tf.layers.dense(final_pool, 1024, activation=tf.nn.relu, name='fc')

#### Summaries

Creation:  

    with tf.name_scope("summaries"):
        tf.summary.scalar("loss", self.loss)
        tf.summary.scalar("accuracy", self.accuracy)
        tf.summary.histogram("Histogram loss", self.loss)
        self.summary_op = tf.summary.merge_all()
        
Run:  

    loss_batch, _, summary = sess.run([model.loss, model.optimizer, model.summary_op], feed_dict=feed_dict)

Write them to file:  

    writer = tf.summary.FileWriter(logdir, sess.graph)
    writer.add_summary(summary, global_step=step)
    
Vizualize them:  

    $ tensorboard --logdir=<LOG_PATH>

## Distributer Computation example

    with tf.device('/gpu:2'):
      a = tf.constant([1.0,2.0,3.0], name='a')
      b = tf.constant([1.0,2.0,3.0], name='b')
      c = tf.matmul(a,b)

## Structure of a model

**Phase 1: Definitions**

     1. Define placeholders for input/output
     2. Define weights
     3. Define inference model
     4. Define loss function
     5. Define optimizer
     
**Phase 2: Training Loop**

     1. Initialize model parameters
     2. Input training data
     3. Execute inference model on training data
     4. Compute loss
     5. Adjust model parameters
     6. GOTO (1)

## TFRecord
Binary file format recommended for TF, makes better use of disk cache.

    # Step 1: create a writer to write tfrecord to that file
    writer = tf.python_io.TFRecordWriter(out_file)
    
    # Step 2: get serialized shape and values of the image
    shape, binary_image = get_image_binary(image_file)
    
    # Step 3: create a tf.train.Features object
    features = tf.train.Features(feature={'label': _int64_feature(label),
    'shape': _bytes_feature(shape),
    'image': _bytes_feature(binary_image)})
    
    # Step 4: create a sample containing of features defined above
    sample = tf.train.Example(features=features)
                                                                            
    # Step 5: write the sample to the tfrecord file
    writer.write(sample.SerializeToString())
    writer.close()
    
## Seq2seq

    outputs, states = basic_rnn_seq2seq(encoder_inputs, decoder_inputs, cell)
    outputs, states = embedding_rnn_seq2seq(...)
    outputs, states = embedding_attention_seq2seq(...)
    outputs, losses = model_with_buckets(...)

# References

* Google Crash Course: Machine Learning
* Stanford Deep Learning Course
