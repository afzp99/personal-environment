#####################################################
# Code Taken & Modified from Google ML Crash Course #
#####################################################

# DOWNLOAD "https://download.mlcc.google.com/mledu-datasets/mnist_train_small.csv"

from __future__ import print_function

#import glob
#import math
import os

#from IPython import display
#from matplotlib import cm
#from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
#import seaborn as sns
#from sklearn import metrics
import tensorflow as tf
#from tensorflow.python.data import Dataset

TEST_FILE="/tmp/mnist_train_small.csv"

def construct_feature_columns():
    """
    Construct the TensorFlow Feature Columns.
    Returns:
      A set of feature columns
    """ 
    # There are 784 pixels in each image.
    return set([tf.feature_column.numeric_column('pixels', shape=784)])

def parse_labels_and_features(dataset):
    """
    Extracts labels and features.
    This is a good place to scale or transform the features if needed.
    Args:
    dataset: A Pandas `Dataframe`, containing the label on the first column and
      monochrome pixel values on the remaining columns, in row major order.
    Returns:
    A `tuple` `(labels, features)`:
      labels: A Pandas `Series`.
      features: A Pandas `DataFrame`.
    """
    labels = dataset[0]
    
    # DataFrame.loc index ranges are inclusive at both ends.
    features = dataset.loc[:,1:784]
    # Scale the data to [0, 1] by dividing out the max value, 255.
    features = features / 255
    return labels, features

if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.ERROR)
    pd.options.display.max_rows = 10
    pd.options.display.float_format = '{:.1f}'.format
    mnist_dataframe = pd.read_csv( TEST_FILE, 
        sep=",",
        header=None)

    # Use just the first 10,000 records for training/validation.
    mnist_dataframe = mnist_dataframe.head(10000)
    mnist_dataframe = mnist_dataframe.reindex(np.random.permutation(mnist_dataframe.index))
    training_targets, training_examples = parse_labels_and_features(mnist_dataframe[:7500])
    #validation_targets, validation_examples = parse_labels_and_features(mnist_dataframe[7500:10000])
    
    rand_example = np.random.choice(training_examples.index)
    _, ax = plt.subplots()
    ax.matshow(training_examples.loc[rand_example].values.reshape(28, 28))
    ax.set_title("Label: %i" % training_targets.loc[rand_example])
    ax.grid(False)
    plt.show()
