import tensorflow as tf

class MyModel(object):
  batch_size = 0
  max_epochs = 0

  
  # Build the graph
  def __init__(self,params):
    self.load_data()
    self.create_placeholders()
    self.create_loss()
    self.create_optimizer()
    self.create_embedding()

  # Load data from disk to memory
  def _load_data(self):
    pass
  
  # Creates feed_dict for training
  def _create_feed_dict(self, labels, values):
    pass
    
  # Define placeholders for feeding Input/Output
  def _create_placeholders(self):
    pass

  # Define Initial Weights
  def _create_embedding(self):
    pass

  # Define inference and loss function
  def _create_loss(self):
    pass

  # Define optimizer
  def _create_optimizer(self):
    pass

  ###########################################
  #          PUBLIC FUNCTIONS               #
  ###########################################

  def predict(input_data, session, input_data):
  """
  Transforms input data into predictions
  Args:
    input_data: Tensor[batch_size, n_features]
  Returns:
    predictions: Tensor[batch_size, n_features]
    average_loss
  """
    pass

  # Calculate error
  def calculate_error(self, predictions, real_values):
    pass

  def train_model(self, session, input_data, input_labels):
  """
  Trains model for 1 epoch (1 full training cycle)
  Returns:
    average_loss
  """
    pass
