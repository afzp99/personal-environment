#!/bin/bash
# Generates values for f(x) = 2x
f(){
 let result=2*$1
 echo $result
}

main(){
    echo "\"x\",\"y\"" > function_values.csv
    for x in `seq $1 $2`;
    do
        echo "$x ,$(f $x)" >> function_values.csv
    done
}


main $1 $2

