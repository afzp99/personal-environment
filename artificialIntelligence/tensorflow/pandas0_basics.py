import pandas as pd
import matplotlib
city_names = pd.Series(['Envigado','Medellin','Bello'])
population = pd.Series([20,30,22])
dataFrame = pd.DataFrame({ 'City name': city_names, 'Population': population })
dataFrame['Ends With O'] = (dataFrame['City name'].apply(lambda name: name.endswith('o')))
print(dataFrame)
print(dataFrame.describe())

print("Indexes're present in Series & Dataframes")
print(city_names.index)
dataFrame.reindex([2,0,1])
#dataFrame.reindex([54,0,1]) and also with not existing indexes

# To import from csv
# DataFrame = pd.read_csv("URL_or_path.csv",sep=",")
