import tensorflow as tf
############################################################
# Usage:
# $ python3.6 tensorflow1_graph.py
# This will generate a directory graph/ with TF graph info.
#
# $ tensorboard --logdir="./graphs" --port 6006
# Enter in localhost:6006 in your browser
############################################################
a = tf.constant(2, name="Constant_a")
b = tf.constant(3, name="b")
x = tf.add(a,b, name="a_plus_b")
y = tf.multiply(a,b, name="a_times_b")

# IMPORTANT: You have to initialize your variables, initialized bellow
variable = tf.Variable([2,3], name="variable_vector")

# IMPORTANT: This operation has to be executed, showed bellow
assign_operation = variable.assign([3,5])

# Data Types
t_0 = 19
tf.ones_like(t_0, name="t_0_1")

t_1 = ['apple', 'mango']
tf.zeros_like(t_1, name="t_1")

# Placeholders
external = tf.placeholder(tf.float32, shape=[2])
constant_1 = tf.constant([2.0,2.0])
placeholder_result = tf.add(external, constant_1)

# Use a variable to initialize another
W = tf.Variable(100)
U = tf.Variable(2*W.initialized_value())

#NOTE: Each  session maintains its own copy of variable
with tf.Session() as sess:
  # Tensorboard
  writer = tf.summary.FileWriter("graphs", sess.graph)
  x,y = sess.run([x,y])
  print(x,y)
  sess.run(variable.initializer)
  print("variable beforeAsign: " + str(variable.eval()))
  sess.run(assign_operation)
  print("variable afterAsign:" + str(variable.eval()))
  print(sess.run(placeholder_result, {external: [16,4]}))
  writer.close()
