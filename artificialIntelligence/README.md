# Machine Learning

## Concepts
**Label:** y  
**Feature:** x  
**LabeledExample:**   (x,y)  
**UnlabeledExample:** (x,?)  
**Model:** f(x)  
**Regression Model:** Predicts continuous values   - Ej:Value of a house  
**Classification Model:** Predicts discrete values - Ej: Mail spam or not  
**One hot encoding:** Ej(Predict if an image is a number - Output encoded answer[10] with only 1 element in 1 & others in 0).  

## Linear Regression
y  = a*x + b  
y' = b + w1*x1  
b = bias = y-intercept = w0  
L² = (y - prediction(x))²  
MSE(MeanSquareError): Average squared loss per example.  

## Cleaning Data

**Scaling:** Convert values from their natural range (100-900) into a standard range (0-1 or -1-1) avoiding NaN traps (Ej: exceed floating-point precision)  
**Handling extreme outliers:** Use logarithmic scaling or clip the feature.  
**Clipping:** Limit the maximum value at an arbitrary value.  
**Binning:** Create bins for values that doesn't have to be floating (Ej: 36<Latitude<=37)  
**Scrubbing:** 

## Data Preprocessing

- Digit normalization
- Sequences too long / too short
- Punctuation
- Remove duplicates (Test only on unseen data)
- Group data into buckets

## Mean normalization

     x_i = ( x_i - u_i ) / s_i
	 
	 u_i -> Average value of x in training set
	 s_i -> max-min (Range for standard deviation)

## Non Linear Problems

**Feature cross:** Synthetic feature that econdes nonlinearity in feature space and joins different features. Ej: [behavior_type X time_of_day ]  

## Regularization in Linear Regression

In order to prevent overfitting, complex models can be penalized. Instead of minimizing loss with the empirical risk minimization: *minimize(Loss(Data|Model))*, minimize loss+complexity*(regularization term)*: *minimize(Loss(Data|Model) + complexity())*.    

Complexity can be seen as a function of the weights of all the features or as a function of the total number of features with nonzero weights.   

Complexity can be quantified using L2 regularization formula (the sum of the squares of all the feature weights): L2=w1²+w2²+...+wn².  
This formula can be modified using a **regularization rate** called **lambda**: *minimize(Loss(Data|Model) + λ x complexity())*

## Logistic Regression
Extremely efficient mechanism for calculating probabilites. Using a *sigmoid function* the model can ensure output always falls between 0 and 1.  

    z = b + w₀x₀ + w₁x₁ + ... + wnxn
    y = 1/(1+e^-z)

The loss function for Logistic Regression is:  

    Log Loss = Σ -ylog(y')-(1-y)log(1-y')
	
That is equivalent to:  

    Cost(h(x)) = -log(h(x)) if y == 1
	           = -log(1-h(x)) if y == 0

#### Hypothesis Function in Logistic Regression
Estimated probability that (y=1), given x, parameterized by Theta (θ)  

    θ' = Transpose of θ
	θ'x = θ_0 + θ1x1 + θ2x2
	h(x) = g(θ'x)
    z = θ'x
	g(z) = 1 / (1 + e^-z)

## Regularization in Logistic Regression
There are three strategies:

**L₂:** Penalizes weight²  
**Early Stopping:** Limitting n° training steps or learning rate.  
**L₁:** Penalizes |weight|. May cause non-informative features to become exactly 0, reducing the model size.  

## Logistic Regression Qualities

**Accuracy (Exactitud):** correctPredictions / totalPredictions. This feature alone doesn't show the correctness of the model. Ej: A deadly desease afflicts 0.01% of the population. An ML model that always predicts "not sick will be 99.99% accurate.  

**Precision:** TruePositives / (TruePositives + FalsePositives). It Measures the proportion of positive predictions that's actually correct.  

**Recall (Exhaustividad):** TruePositives / (TruePositives + FalseNegatives). It Measures the proportion of actual positive that get identified.  

**ROC curve (Receiver Operating Characterisitc):** Shows the performance at all thresholds. Recieves two parameters: TPR & TPR.  

**True Positive Rate (TPR):** Same as Recall  

**False Positive Rate (FPR):** *FP / (FP + TN)*  

**Area Under the ROC Curve (AUC):** Efficient, sorting-based algorithm that can provide info about ROC. Represents the probability of
doing correct positive predictions.  

## Support Vector Machine (SVM)

Also called large margin classifier. It's loss equation is a slightly modified logistic regression loss eq version.

**Kernel:** Given *x*, compute new features depending on proximity to landmarks (points of reference)

     f1 = similarity(x,l_1)
	 l_1 = landmark_1

     If x ≈ l_1:
	     f1 ≈ 1
	 If x is far from l_1:
	     f1 ≈ 0
		 
**No kernel(Linnear Kernel):** Good when number of features is large and there are few examples.  
**Gaussian Kernel:** Good when number of features is small and/or there are many examples.  

	f = exp(-||x-l(i)||² / 2σ²)
	
**NOTE:** Scale features before using Gaussian Kernel (feature 1 around 1000 m² and feature 2 around 1 bedroom).  
		 
Parameter **C** is equivalent to **1/lambda** in regularization effects (Small C -> (large lambda) -> Higher bias -> Low variance -> Underfitting).  

**Usage:** Choose a C parameter and a kernel (similarity function).  


# References
Machine Learning Crash Course - Google  
