Convolutional Neural Networks
=============================

Layer Size
----------

**(n)** size of previous layer
**(p)** Padding size
**(f)** Filter size
**(s)** Stride size

    length = (n + 2p - f)/s + 1

This length has to be calculated for each axis (sometimes height differs from weigth)

Benefits
========

**Parameter sharing:** A kernel is a small feature detector that doesn't need to be too
big because it's function is to detect forms in a portion of the image.

**Sparsity of connections:** Each output value depends only on a small number of inputs (the ones that surrounds it).

## Convultional Networks

ConvNet architectures make the explicit assumption that the inputs are images. 
It's a sequence of layers that transforms one volume of activation to another through a differentiable function.  

Example Architecture:  

    INPUT: [32x32x3] RGB Image 32x32
    CONV: [32x32xN] with N classes (Can be reduced in size increasing the Stride)
    RELU: max(0,n)
    POOL: [16x16xN] Reduces the spatial dimentions (width/height)
    FC (FullyConected): [1x1x10] With the class scores predicted.

#### Convultional Layer
Convultion operation performs dot products between the filters and subsections of the input.  

Parameters:  

    K -> n° Filters
    F -> Kernel (Filter) size
    S -> Stride (Zancada)
    P -> Amount of zero padding

#### Pooling Layer
Reduces spatial size applying replacing a subregion of data with  its maximal value (Could be MAX or AVG)

    1 0 2 4
    0 5 1 0    ->  1 0  -> MAX -> 4 -> ...  ->  5 4
    4 2 3 0        2 4                          9 3
    0 9 0 1
    
Parameters:

    F -> spatial extent
    S -> Stride

## Architectures ##

### 1) LeNet-5 ###

**Activation Function:** Sigmoid

| Layer              | Parameters           | Size               |
|--------------------|----------------------|--------------------|
| INPUT              |                      | 32x32x1            |
| Conv AvgPool Activ | f=5x5 s=1 -> f=2 s=2 | 28x28x6 > 14x14x6  |
| Conv AvgPool Activ | f=5x5 s=1 -> f=2 s=2 | 10x10x16 > 5x5x516 |
| Fully Connected    |                      | 120                |
| Fully Connected    |                      | 84                 |
| OUTPUT             |                      | 10 (0..9 Digits)   |

### 2) AlexNet ###

**Activation Function:** ReLU

| Layer           | Parameters             | Size                  |
|-----------------|------------------------|-----------------------|
| INPUT           |                        | 227x227x3             |
| Conv MaxPool    | f=11x11 s=4 -> f=3 s=2 | 55x55x96  > 27x27x96  |
| Conv MaxPool    | f=5x5  SAME -> f=3 s=2 | 27x27x256 > 13x13x256 |
| Conv            | f=3x3  SAME            | 13x13x384             |
| Conv            | f=3x3  SAME            | 13x13x384             |
| Conv MaxPool    | f=3x3  SAME            | 13x13x256 > 6x6x256   |
| Fully Connected |                        | 9216                  |
| Fully Connected |                        | 4096                  |
| Fully Connected |                        | 4096                  |
| Softmax         |                        | 1000                  |


### 2) VGG ###

**Activation Function:** ReLU

| Layer           | Parameters            | Size                    |
|-----------------|-----------------------|-------------------------|
| INPUT           |                       | 224x224x3               |
| Conv X2 MaxPool | f=3x3 SAME -> f=2 s=2 | 224x224x64 > 112x112x64 |
| ...             | ...                   | ...                     |
| Conv X2 MaxPool | f=3x3 SAME -> f=2 s=2 | 14x14x512 > 7x7x512     |
| Fully Connected |                       | 4096                    |
| Fully Connected |                       | 4096                    |
| Softmax         |                       | 1000                    |

## ResNets ##

**Residual block**

    a[l]
	z[l+1] = W[l+1] a[l] + b[l+1]
	a[l+1] = g(z[l+1])
	z[l+2] = W[l+2] a[l+1] + b[l+2]
	a[l+2] = g(z[l+2] + a[l])
	
The presence of a[l] two layer forward is called **shortcut** or **skip-connection**.  

If a[l] shape is different from z[l+2] shape, we add a new Weight matrix as follows:  

    a[l+2] = g(z[l+2] + Ws*a[l])

## Shrink Dimentions ##

**# of Filters:** 1x1 Convolution -> 10x10x150 * 1x1x30 -> 10x10x30
**Other dimentions:** Pooling layer.

## YOLO ##

**New Output Layer:** [ confidence x y w h ] one per grid subsection

**Intersection Over Union:** Measure of overlap between two boxes (window positions).

#### Style transfer
**Content Loss:** Loss between feature maps in the CONTENT layer and the context image.  
**Style Loss:** Loss between the feature maps in the STYLE layer and the style image.  

# References
* Convolutional Neural Networks - Coursera (deeplearning.ai)  
* Stanford Deep Learning Course  
