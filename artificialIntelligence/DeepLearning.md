# Deep Learning

## Deep Neural Networks

The matrices dimentions in the layer **l** are:

    w[l] = [ n[l] , n[l-1] ]
	b[l] = [ n[l] ,    1   ]
	
But, with **m** examples, we have:

    W[l] = [ n[l] , n[l-1] ]
	b[l] = [ n[l] ,    1   ]
	Z[l] = [ n[l] ,    m   ] = W@X + b <- forecasting becames b to [ n, m ]

## Random initialization

Xavier random initialization multiplies every random weight by:

    sqrt(2/n[l-1])
	
This factor takes into account the number of neurons in the previous layer.

## Dropout Regularization

Randomly "drops out" unit activations for a single gradient step.  

## Optimization Methods

**GD with Momentum:** Update rule:

    v_dW = β(v_dW) + (1-β) dW
    W = W - a v_dW

**GD with Momentum with bias correction:**

    v_dW = β(v_dW) + (1-β) dW
	v_dW = v_dW / (1-β^t)
    W = W - a v_dW

**RMSprop:** Update rule is modified:

    s_dW = β(s_dW) + (1-β) dW² (square element wise)
    W = W - a * dW / (√(s_dW) + ϵ)
	
**Adam:** It's a combination of Momentum and RMSprop, both with bias correction:

    v_dW = β₁(v_dW) + (1-β₁) dW
	v_dW = v_dW / (1-β₁^t)
	
	s_dW = β₂(s_dW) + (1-β₂) dW²  (square element wise)
	s_dW = s_dW / (1-β₂^t)

    W = W - a * v_dW / (√(sdW) + ϵ)

## Embeddings
Translates large sparse vectors into a lower-dimensional space that preserves semantic relationships.  
When learning **D**-dimensional embedding, each element is represented by **D** real-valued numbers, each one giving the coordinate in 1 dimension.  
**Latent dimension:** Represents a feature that is inferred from the data (not explicit).

## Gated Recurrent Units
Using a gating mechanism to control entry and emition of information.  

    input gate: how much of input to let through.
    forget gate: how much of previous state to take into account.
    output gate: how much of hidden state to expose to the next step.

## Reinforcement Learning
**Reward function:** Tell us which states and actions are better.

# References
Machine Learning Crash Course - Google  
Stanford Deep Learning Course  
