## Neural Networks

*1. Set of nodes organized in layers (like neurons).*  

Neural networks gives us a way to learn nonlinear models without using feature crosses explicitly.  

**Hidden Layers:** Intermediary values, weighted sum of input node values. The output should be
also a weighted sum of the yellow nodes. Adding a second layer will not make the function non-linear per se.  

**Activation Function:** Introduces directly a nonlinearity. It's used to pipe the hidden layer through nonlinear
functions, before beeing passed to the next layer.  

*Sigmoid activation function*  

    F(x) = 1 / (1 + e^(-x))

*Rectified Linear Unit (ReLU)*  

    F(x) = max(0,x)

## Forward Propagation

Starts from the activations of the input layer and forward propagate 
that to the first hidden layer, then the second hidden layer, and then
finally the output layer.

	a(1) = x
	z(2) = θ(1)a(1)
	a(2) = g(z(2))  (add a_0(2))
	...
	a(4) = h(x) = g(z(4))

## BackPropagation

Most common training algorithm for neural networks.  

*Common Troubleshooting -> Consequences -> Solution*

    Very small gradients for lower layers (Deep networks)  ->  Very slow training in low layers -> ReLU activation.
    Very large weights -> Gradients too large to converge -> Batch normalization can prevent exploding gradients & Lowering learning rate.

Error of node j in output layer

	δĵ(n) = aĵ(n) - yĵ
	
Error of node j in hidden layer

	δĵ(n) = θ(n)' δ(n+1) * g'(z(n))
	g'(z(n)) = a(n)*(1-a(n))

**NOTE:** There is no δ(1), we only calculate 
the error for hidden & output layers.  

**Algorithm**

    ∆(l) = [ 0 ... 0 ]
	for i in (0..m)
		a(1) = x(i)
		a(l) = forwardProp()
		δ(L) = a(l) - y(i)
		δ(2..L-1) = backProp()
		∆(l) = ∆(l) + δ(L+1) * (a(l))T
	D(l) = ∆(l)/m + lambda*θ
	
∂J(θ)/∂θ(L) = D(l)
# The Problem of Symmetric Ways #

Initializing the weights in the same value (Ej. 0) produces redundant neurons (Repeated training).
This symmetry is broken by a random initialization of the weights.

## Training a Neural Network

1. Randombly initialize weights  
2. h(x) = forwardPropagation()  
3. J(θ) -> cost function  
4. ∂J(θ)/∂θ (GRADIENT) -> backPropagation()  
5. Compare backPropagation gradient with a numerical estimate gradient.  

	    J(θ+e)-J(θ-e) / 2e
		
6. Use optimization method with backPropagation to minimize J(θ).  

# References
Machine Learning Crash Course - Google  
Stanford Deep Learning Course  
Coursera - Machine Learning Course - Andrew NG  
