# Sequence Models #

## Examples of sequence data ##

* Speech recognition
* Music generation
* Sentiment classification
* DNA sequence analysis
* Machine translation
* Video activity recognition
* Name entity recognition

## Words representation ##

Words are represented using an enumeration with a given vocabulary.

## Why not a Standard Network ##

* Inputs & Outputs can be different lengths in different examples.
* Standard NN doesn't share features learned across different positions.

## Forward Propagation ##

    a[t] = g(w_aa @ a[t-1] + w_ax @ x[t] + b_a) <- tanh / ReLU
	y[t] = g(w_ya @ a[t] + b_y)             <- sigmoid


## RNN Types ##

* One to One ()
* One to Many (Sequence generation)
* Many to One (Sentiment classification)
* Many to Many (Encoder - Decoder - Language Translation)
* Many to Many ()

## Tokenize sequence elements ##

* Each sequence element (vocabulary entry) is tokenized.
* UNKNOWN Token for elements that doesn't belongs to the known vocabulary.
* EndOfSentence EOF Token to limit secuence length.

## Gated Recurrent Unit (GRU) ##

**Simplified Version**  

The gate Γ_u will be a value between 0 and 1, and will decide when to update the Memory cell or not.  

    c = Memory Cell
	c<t> = a<t>
	Γ_u = σ(w_u[c<t-1>,x<t>] + b_u)
	c' = tanh(w_c[c<t-1>,x<t>] + b_c)
	c<t> = Γ_u * c' + (1 - Γ_u) * c<t-1>

**Full GRU**  

The gate Γ_r will decide when to forget or not.  

    c' = tanh(w_c[ Γ_r * c<t-1> , x<t> ] + b_c)
	Γ_u = σ(w_u[c<t-1>,x<t>] + b_u)
	Γ_r = σ(w_r[c<t-1>,x<t>] + b_r)
	c<t> = Γ_u * c' + (1 - Γ_u) * c<t-1>

## Long Short Term Memory (LSTM) ##

This architecture separates the Forget factor from the Update gate, and adds a new Output gate.

    c' = tanh(w_c[ a<t-1> , x<t> ] + b_c)
	Γ_u = σ(w_u[a<t-1>,x<t>] + b_u)
	Γ_f = σ(w_f[a<t-1>,x<t>] + b_f)
	Γ_o = σ(w_o[a<t-1>,x<t>] + b_o)
	c<t> = Γ_u * c' + Γ_f * c<t-1>
	a<t> = Γ_o * tanh(c<t>)

## Attention Model ##

* **a<t,t'>** amount of attention y<t> should pay to a<t'>
