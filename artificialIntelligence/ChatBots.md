# ChatBots with IBM Watson

## Components ##

**Intents:** Determine what the user wants *(#greetings: Hello, Hi)*  
**Entities:** Capture specific values in user input *(@location: Envigado -> Cra43...)*  
**Dialog:** Responses depending of intents and entities detected.
**Slots:** Stored variables that represents permanent context information.


